# dmenu-border

Fixed dmenu-border patch for dmenu. Works with version 5.0 - 5.1

The dmenu-border patch on suckless.org has a bug — the right side of the
window is rendered off screen because the width of the border is not taken into
account. The same occurs with the bottom and right side border when `topbar = 0`.
This patch fixes that bug, as shown below.

**NOTE:**
- These patches were generated with a stock version of dmenu 5.1, with no other
  patches applied. They are also compatible with version 5.0.
- The `dmenu-border-5.1.diff` patch is not directly compatible with the
  [center patch](https://tools.suckless.org/dmenu/patches/center/).
- The `dmenu-border-center-5.1.diff` patch combines `dmenu-border-5.1.diff` with
  [center](https://tools.suckless.org/dmenu/patches/center/) so you don't need
  to resolve the conflict.

## Version with border bug
![dmenu with buggy border](buggy.png)

## Fixed version
![dmenu with fixed border](fixed.png)
